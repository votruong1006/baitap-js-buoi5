//bài1
function tinhDiem() {
  console.log("helo");
  var diemChuan = document.getElementById("diemchuan").value * 1;
  var diem1 = document.getElementById("diemmon1").value * 1;
  var diem2 = document.getElementById("diemmon2").value * 1;
  var diem3 = document.getElementById("diemmon3").value * 1;
  var khuVuc = document.getElementById("khuvuc").value;
  var doiTuong = document.getElementById("doituong").value;
  var sum = diem1 + diem2 + diem3;
  if (diem1 == 0 || diem2 == 0 || diem3 == 0) {
    return (document.getElementById(
      "result"
    ).innerHTML = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`);
  } else {
    if (khuVuc == "A") {
      sum = sum + 2;
    } else if (khuVuc == "B") {
      sum = sum + 1;
    } else if (khuVuc == "C") {
      sum = sum + 0.5;
    } else {
      sum = sum + 0;
    }
    if (doiTuong == "1") {
      sum = sum + 2.5;
    } else if (doiTuong == "2") {
      sum = sum + 1.5;
    } else if (doiTuong == "3") {
      sum = sum + 1;
    } else {
      sum = sum + 0;
    }
    if (sum >= diemChuan) {
      document.getElementById(
        "result"
      ).innerHTML = `Bạn đã ĐẬU <br /> Tổng điểm: ${sum}  `;
    } else {
      document.getElementById(
        "result"
      ).innerHTML = `Bạn đã RỚT <br /> Tổng điểm: ${sum}  `;
    }
  }
  //input:điểm chuẩn,điểm moon1,điểm moon2,điểm moon3, khu vực, đối tượng
  // các bước xử lí:
  //      b1: tạo biến diemchuan,diem1,diem2,diem3,khuvuc,doituong,sum
  //      b2: gán giá trị cho biến từng biến trong đó sum=diem1+diem2+diem3
  //      b3:ta dùng if-else để giải quyết
  //      b4:trong if ta cho điều kiện nếu diem1 == 0 || diem2 == 0 || diem3 == 0 rồi ta in rs msnf hình `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`
  //      b5:trong else ta dùng if-else-if để tính sum theo từng khu vực nếu khu vực là A hoặc B hoặc C thì ta cộng sum thêm lần lược 2 hoặc 1 hoặc 0.5 nếu ko có khu vực thì cộng 0
  //      b6:ta dùng if-else-if để tính sum theo từng đối tượng nếu đối tượng là 1 hoặc 2 hoặc 3 thì ta cộng sum thêm lần lược 2.5 hoặc 1.5 hoặc 1 nếu ko có khu vực thì cộng 0
  //      b7:ta dungg if-else để đưa kq ra màn hình nếu  sum >= diemChuan thì in ra màn hình `Bạn đã ĐẬU <br /> Tổng điểm: ${sum}` còn ko thì in ra `Bạn đã RỚT <br /> Tổng điểm: ${sum}  `
  //output:kết quả đậu hay rớt và tổng điểm
}

// bài 2
function tinhTienDien() {
  var hoTen = document.getElementById("ho-ten").value;
  var soKw = document.getElementById("so-kw").value * 1;
  var tienDien = 0;
  if (soKw <= 0) {
    document.getElementById(
      "result1"
    ).innerHTML = `Xin nhập đúng và đầy đủ dữ liệu`;
  } else {
    if (soKw <= 50) {
      tienDien = soKw * 500;
    } else if (soKw <= 100) {
      tienDien = 50 * 500 + 650 * (soKw - 50);
    } else if (soKw <= 200) {
      tienDien = 50 * 500 + 650 * 50 + 850 * (soKw - 100);
    } else if (soKw <= 350) {
      tienDien = 50 * 500 + 650 * 50 + 850 * 100 + 1100 * (soKw - 200);
    } else {
      tienDien =
        50 * 500 + 650 * 50 + 850 * 100 + 1100 * 150 + 1300 * (soKw - 350);
    }
    document.getElementById(
      "result1"
    ).innerHTML = `Họ tên: ${hoTen}<br/> Tiền điện: ${new Intl.NumberFormat(
      "de-DE"
    ).format(tienDien)} đ`;
  }
  //input:họ tên và số kw người dùng nhập vào
  //các bước thực hiênj:
  //  b1:tạo biến hoTen,soKw,tienDien
  //  b2:gán giá trị cho các biến   ,tienDien=0
  //  b3:ta dùng if-else để giải quyết trong if ta cho đk soKw <= 0 và in ra màn hình `Xin nhập đúng và đầy đủ dữ liệu` đây là trường hợp người dùng ko nhập thông tin
  //  b4:trong else ta dùng if-else-if để tính tiendien theo các đk lần lượt là soKw <= 50 ta tính theo công thức tienDien = soKw * 500
  //                                                                            soKw <= 100 ta tính theo công thức tienDien = 50 * 500 + 650 * (soKw - 50)
  //                                                                            soKw <= 200 ta tính theo công thức tienDien = 50 * 500 + 650 * 50 + 850 * (soKw - 100)
  //                                                                            soKw <= 350 ta tính theo công thức tienDien = 50 * 500 + 650 * 50 + 850 * 100 + 1100 * (soKw - 200)
  //                                                                            soKw > 350 ta tính theo công thức tienDien =50 * 500 + 650 * 50 + 850 * 100 + 1100 * 150 + 1300 * (soKw - 350);
  //  b5:in họ tên và kq tiendien ra màn hình
  //output: họ tên và số tiền điện .
}

//bài3
function tinhTienThue() {
  var hoTen = document.getElementById("hovaten").value;
  var thuNhap = document.getElementById("thu-nhap").value * 1;
  var soNguoi = document.getElementById("so-nguoi").value * 1;
  var tienChiuThue = thuNhap - 4e6 - soNguoi * 1.6e6;
  var tienThue = 0;
  if (thuNhap < 10e6) {
    document.getElementById(
      "result2"
    ).innerHTML = `Số tiền thu nhập phải từ 10 triệu trở lên.`;
  } else {
    if (tienChiuThue <= 60e6) {
      tienThue = tienChiuThue * 0.05;
    } else if (tienChiuThue <= 120e6) {
      tienThue = 60e6 * 0.05 + (tienChiuThue - 60e6) * 0.1;
    } else if (tienChiuThue <= 210e6) {
      tienThue = 60e6 * 0.05 + 60e6 * 0.1 + (tienChiuThue - 120e6) * 0.15;
    } else if (tienChiuThue <= 384e6) {
      tienThue =
        60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + (tienChiuThue - 210e6) * 0.2;
    } else if (tienChiuThue <= 624e6) {
      tienThue =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        (tienChiuThue - 384e6) * 0.25;
    } else if (tienChiuThue <= 960e6) {
      tienThue =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        240e6 * 0.25 +
        (tienChiuThue - 624e6) * 0.3;
    } else {
      tienThue =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        240e6 * 0.25 +
        336e6 * 0.3 +
        (tienChiuThue - 960e6) * 0.35;
    }
    document.getElementById(
      "result2"
    ).innerHTML = `Họ tên: ${hoTen} <br/> Tiền thuế thu nhập cá nhân: ${new Intl.NumberFormat(
      "de-DE"
    ).format(tienThue)} VNĐ`;
  }
  //input:họ tên ,tổng thu nhập và số người phụ thuộc
  //các bước thực hiênj:
  //  b1:tạo biến hoTen,thunhap,songuoi,tienthue,tienchiuthue
  //  b2:gán giá trị cho các biến   ,tienthue=0,tienchiuthue=thuNhap - 4e6 - soNguoi * 1.6e6
  //  b3:ta dùng if-else để giải quyết trong if ta cho đk thuNhap < 10e6 và in ra màn hình `Số tiền thu nhập phải từ 10 triệu trở lên.` đây là trường hợp người dùng ko nhập thông tin
  //  b4:trong else ta dùng if-else-if để tính tienthue theo các đk lần lượt là tienChiuThue <= 60e6 ta tính theo công thức tienThue = tienChiuThue * 0.05
  //                                                                            tienChiuThue <= 120e6 ta tính theo công thức tienThue = 60e6 * 0.05 + (tienChiuThue - 60e6) * 0.1
  //                                                                            tienChiuThue <= 210e6 ta tính theo công thức tienThue = 60e6 * 0.05 + 60e6 * 0.1 + (tienChiuThue - 120e6) * 0.15
  //                                                                            tienChiuThue <= 384e6 ta tính theo công thức tienThue =60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + (tienChiuThue - 210e6) * 0.2;
  //                                                                            tienChiuThue <= 624e6 ta tính theo công thức tienThue =60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + 174e6 * 0.2 +(tienChiuThue - 384e6) * 0.25
  //                                                                            tienChiuThue <= 960e6 ta tính theo công thức tienThue =60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + 174e6 * 0.2 +240e6 * 0.25 +(tienChiuThue - 624e6) * 0.3;
  //                                                                            tienChiuThue > 960e6 ta tính theo công thức tienThue =60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + 174e6 * 0.2 +240e6 * 0.25 +336e6 * 0.3 +(tienChiuThue - 960e6) * 0.35;
  //  b5:in họ tên và kq tienthue ra màn hình
  //output: họ tên và tiền thuế thu nhập cá nhân .
}

//bài4
function myFuntion() {
  var khach = document.getElementById("loaikhach").value;

  if (khach == "DN") {
    document.getElementById("ketnoi").style.display = "block";
  } else {
    document.getElementById("ketnoi").style.display = "none";
  }
}
function tinhTienCap() {
  var khachh = document.getElementById("loaikhach").value;
  var soKetNoi = document.getElementById("soketnoi").value * 1;
  var maKH = document.getElementById("makhachhang").value * 1;
  var soKenh = document.getElementById("sokenh").value * 1;
  var tienCap = 0;

  if (soKenh == 0 || maKH == 0) {
    document.getElementById(
      "result3"
    ).innerHTML = `Hãy điền đầy đủ thông tin. `;
  } else {
    if (khachh == "DN") {
      if (soKetNoi <= 10) {
        tienCap = 15 + 50 * soKenh + 75;
      } else {
        tienCap = 15 + 50 * soKenh + 75 + (soKetNoi - 10) * 5;
      }
    } else if (khachh == "ND") {
      tienCap = 4.5 + 20.5 + 7.5 * soKenh;
    }
    document.getElementById(
      "result3"
    ).innerHTML = `Mã khách hàng: ${maKH} <br/> Tiền cáp: ${new Intl.NumberFormat(
      "de-DE"
    ).format(tienCap)} $`;
  }
  //input:loại khách hàng,mã khachs hàng,số kênh cao cấp,số kết nối
  //các bước xử lí:
  //  b1:tạo các biến khachh,soketnoi,maKH,sokenhvà tienCap
  //  b2: gán giá trị cho cho các biến và tienCap=0
  //  b3:ta dùng if-else để giải quyết trong if ta cho đk soKenh == 0 || maKH == 0 thì ta in ra màn hình `Hãy điền đầy đủ thông tin. đây là khi người dùng ko nhập thông tin
  //  b4:trong else ta dùng lệnh if-else lồng nhau để tính tienCap trong if ta cho dk khachh == "DN" thì ta tính tienCap theo đk soKetNoi <= 10 là tienCap = 15 + 50 * soKenh + 75; nếu soKetNoi > 10 thì tienCap = 15 + 50 * soKenh + 75 + (soKetNoi - 10) * 5
  //                                                                                  khachh == "ND" thì tienCap = 4.5 + 20.5 + 7.5 * soKenh
  //  b5:ta in mã khách hàng maKH và tiền cáp tienCap ra màn hình
  //output: mã khách hàng đã nhập cùng với số tiền cáp
}
